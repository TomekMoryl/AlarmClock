package pl.ramalash.alarmclock;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

/**
 * Created by Tomek on 2017-04-25.
 */
public class App {

    public static void main(String[] args) throws IOException {
        Clock clock = new Clock();
        List<Alarm> alarms = clock.getAlarms();
        Alarm alarm = new Alarm();
        Alarm alarm2 = new Alarm();
        Alarm alarm3 = new Alarm();
        Alarm alarm4 = new Alarm();
        Alarm alarm5 = new Alarm();
        alarm.setTime("18:30");
        clock.addAlarm(alarm);
        alarm2.setTime("23:01");
        clock.addAlarm(alarm2);
        alarm3.setTime("11:55");
        clock.addAlarm(alarm3);
        alarm4.setTime("04:13");
        clock.addAlarm(alarm4);
        alarm5.setTime("01:47");
        clock.addAlarm(alarm5);
        System.out.println("Sorted Ascending");
        Collections.sort(alarms);
        clock.printInfo();
        System.out.println("Sorted Descending");
        Collections.sort(alarms, Collections.reverseOrder());
        Thread thread = new Thread(clock);
        thread.start();

    }
}
