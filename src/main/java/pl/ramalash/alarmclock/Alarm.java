package pl.ramalash.alarmclock;

import java.time.LocalTime;

/**
 * Created by Tomek on 2017-04-26.
 */
public class Alarm implements Comparable<Alarm> {
    private LocalTime time;

    public void setTime(String time) {
        this.time = LocalTime.parse(time);
    }

    public LocalTime getTime() {
        return time;
    }

    public void trigger() {
        System.out.println("Alarm");
    }

    @Override
    public String toString() {
        return getTime().toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }

        if (obj instanceof LocalTime) {
            LocalTime other = (LocalTime) obj;
            return time.getHour() == other.getHour() && time.getMinute() == other.getMinute();
        }
        return false;
    }

    @Override
    public int compareTo(Alarm o) {
        int timeCompare = time.compareTo(o.getTime());
        if (timeCompare != 0) {
            return timeCompare;
        }
        return time.compareTo(o.getTime());
    }
}



