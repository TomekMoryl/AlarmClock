package pl.ramalash.alarmclock;

import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.TimeInfo;

import java.io.IOException;
import java.net.InetAddress;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Created by Tomek on 2017-05-12.
 */
public class Server {
    public TimeInfo timeInfo(String serverName) {
        try{
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(serverName);
        return timeClient.getTime(inetAddress);}
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public long timeParse(TimeInfo timeParse) {
        return timeParse.getReturnTime();
    }

    public LocalTime getTime(long time) {
        Date date = new Date(time);
        Instant instant = Instant.ofEpochMilli(date.getTime());
        LocalTime res = LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime();
        return res;
    }
}
