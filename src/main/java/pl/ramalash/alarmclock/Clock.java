package pl.ramalash.alarmclock;

import org.apache.commons.net.ntp.TimeInfo;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

/**
 * Created by Tomek on 2017-05-06.
 */
public class Clock implements Runnable {
    private List<Alarm> alarms;
    Server server = new Server();

    public Clock() throws IOException {
        alarms = new ArrayList<>();
    }

    public void addAlarm(Alarm alarm) {
        alarms.add(alarm);
    }

    public void printInfo() {
        for (Alarm alarm : alarms) {
            System.out.println(alarm.toString());
        }
    }

    public List<Alarm> getAlarms() {
        return alarms;
    }

    @Override
    public void run() {
        while (true) {
            TimeInfo serverName = server.timeInfo("time-a.nist.gov");
            long time = server.timeParse(serverName);
            LocalTime now = server.getTime(time);
            System.out.println("Server Time: " + now);
            for (Alarm alarm : alarms) {
                System.out.println(alarm.toString());
                if (alarm.equals(now)) {
                    alarm.trigger();
                }
            }
            try {
                Thread.sleep(10 * 1000);
            } catch (InterruptedException e) {
            }
        }
    }

}

